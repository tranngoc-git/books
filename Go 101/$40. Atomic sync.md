# Atomic Operations Provided in The sync/atomic Standard Package

Atomic operations are more primitive than other synchronization techniques. They are lockless and generally implemented directly at hardware level.

## Overview

They sync/atomic standard package provides the following five atomic functions for an integer type T, where T must be any of int32, int64, uint32, uint64 and uintptr

```go
func AddT(addr *T, delta T)(new T)
func LoadT(addr *T) (val T)
func StoreT(addr *T, val T)
func SwapT(addr *T, new T) (old T)
func CompareAndSwapT(addr *T, old, new T) (swapped bool)
```

## Atomic Operations for Integers

Atomic operations guarantee that there are no data races among these goroutines.

```go
	var n int32
	var wg sync.WaitGroup

	for i := 0; i < 1000; i++ {
		wg.Add(1)
		go func() {
			atomic.AddInt32(&n, 1)
			wg.Done()
		}()
	}

	wg.Wait()

	fmt.Println(atomic.LoadInt32(&n))
```

The StoreT and LoadT atomic functions are often used to implement the setter and getter methods of a type if the values of the type need to be used concurrently.

```go

```